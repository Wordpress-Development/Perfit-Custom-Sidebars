<?php

/**
 * Fired during plugin deactivation
 *
 * @link       github.com/austinvernsonger
 * @since      1.0.0
 *
 * @package    Pcs
 * @subpackage Pcs/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pcs
 * @subpackage Pcs/includes
 * @author     Austin Vern Songer <austinvernsonger@protonmail.com>
 */
class Pcs_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
