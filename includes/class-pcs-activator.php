<?php

/**
 * Fired during plugin activation
 *
 * @link       github.com/austinvernsonger
 * @since      1.0.0
 *
 * @package    Pcs
 * @subpackage Pcs/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pcs
 * @subpackage Pcs/includes
 * @author     Austin Vern Songer <austinvernsonger@protonmail.com>
 */
class Pcs_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
